> Welcome to the styleguide for the F3 design system, a collection of visual elements that make up the Fridays for Future brand.

This styleguide is _work-in-progress_, and any feedback is welcome.

For more information, contact David at f3@davidjablonski.at.