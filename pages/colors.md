> In addition to our signature green, we use a variety of colors for highlights, call-to-actions and data visualisation. Keep to this palette to ensure a unified look across all FFF communication.

```color-palette|span-4
colors:
  - {name: "100 - Green", value: "#CDFAD7"}
  - {name: "200 - Green", value: "#8AE5A1"}
  - {name: "300 - Green", value: "#40D46A"}
  - {name: "400 - Green", value: "#1DA648"}
  - {name: "500 - Green", value: "#1D9246"}
  - {name: "600 - Green – Main FFF Brand Color", value: "#1B6839"}
  - {name: "700 - Green", value: "#15482A"}
  - {name: "800 - Green", value: "#06331B"}
  - {name: "900 - Green", value: "#031F11"}
```
```color-palette|span-2
colors:
  - {name: "100 - Neutral", value: "#FFFDF6"}
  - {name: "200 - Neutral", value: "#FAF6E9"}
  - {name: "300 - Neutral", value: "#EEE8D6"}
  - {name: "400 - Neutral", value: "#CAC6B6"}
  - {name: "500 - Neutral", value: "#9A9688"}
  - {name: "600 - Neutral", value: "#6A665A"}
  - {name: "700 - Neutral", value: "#524F44"}
  - {name: "800 - Neutral", value: "#3A392E"}
  - {name: "900 - Neutral", value: "#1A1918"}
```

_More colors to be added soon._