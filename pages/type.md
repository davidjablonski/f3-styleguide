> Our main type family is Jost* by Indestructable Type. It’s used for headlines, but not for body text.

For body text, we use a variety of typefaces, depending on the use case. On our website, we use the system font stack, meaning we use whatever font is the standard on the user’s device. This increases performance and readability.

_Work-in-Progress_

#### Download

```download
title: Download Jost
subtitle: OTF
url: https://indestructibletype.com/Jost.html
```

```type
{
  "color": "#00263e",
  "font": "Jost",
  "kafka": 1,
  "headings": [
    { "label": "Headline 1", "value": 48 },
    { "label": "Headline 2", "value": 32 },
	{ "label": "Headline 3 Label", "value": 22, "tracking": 10}
  ],
}
```



